package com.android2.continente.util;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtil {

    static String TAG = "NetworkUtil";

    static String CONTINENTE_URL = null;
    final static String AFRICA_URL = "https://restcountries.eu/rest/v2/region/Africa";
    final static String AMERICAS_URL = "https://restcountries.eu/rest/v2/region/Americas";
    final static String ASIA_URL = "https://restcountries.eu/rest/v2/region/Asia";
    final static String EUROPE_URL = "https://restcountries.eu/rest/v2/region/Europe";
    final static String OCEANIA_URL = "https://restcountries.eu/rest/v2/region/Oceania";

    public static URL buildUrlContinente(String continente) {

        switch (continente) {
            case "Africa":
                CONTINENTE_URL = AFRICA_URL;
                break;
            case "Americas":
                CONTINENTE_URL = AMERICAS_URL;
                break;
            case "Asia":
                CONTINENTE_URL = ASIA_URL;
                break;
            case "Europa":
                CONTINENTE_URL = EUROPE_URL;
                break;
            case "Oceania":
                CONTINENTE_URL = OCEANIA_URL;
        }
        Uri buildUri = Uri.parse(CONTINENTE_URL).buildUpon().build();
        URL url = null;
        try {
            url = new URL(buildUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        Log.d(TAG, "metodo getResponseFromHttpUrl");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }

}
