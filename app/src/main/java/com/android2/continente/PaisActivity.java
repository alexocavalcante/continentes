package com.android2.continente;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android2.continente.model.Currency;
import com.android2.continente.model.Pais;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PaisActivity extends AppCompatActivity {

    ImageView imageView;
    TextView nome;
    TextView capital;
    TextView regiao;
    TextView populacao;
    TextView latlog;
    TextView moeda;
    Button favorito;
    Pais pais;
    List<Currency> currency = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pais);

        imageView = findViewById(R.id.imageView);
        nome = findViewById(R.id.tv_nome);
        capital = findViewById(R.id.tv_capital);
        regiao = findViewById(R.id.tv_regiao);
        populacao = findViewById(R.id.tv_populacao);
        moeda = findViewById(R.id.tv_moeda);
        latlog = findViewById(R.id.tv_laglog);
        favorito = findViewById(R.id.bt_favorito);

        Intent intent = getIntent();
        //    getSupportActionBar().setTitle(intent.getStringExtra("Continente"));
        pais = (Pais) getIntent().getSerializableExtra("pais");

        nome.setText(pais.getName());
        capital.setText(pais.getCapital());
        regiao.setText(pais.getRegion());
        populacao.setText(pais.getPopulation().toString());
        currency = pais.getCurrencies();
        moeda.setText(currency.get(0).getName());
        latlog.setText(pais.getLatlng().toString());

        Picasso.with(this)
                .load("https://www.countryflags.io/"+pais.getAlpha2Code()+"/flat/64.png")
                .error(R.drawable.ic_error_black_24dp)
                .placeholder(R.drawable.progress_animation)
                .into(imageView, new Callback() {

                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        imageView.setImageResource(R.drawable.ic_error_black_24dp);
                    }
                });



        getSupportActionBar().setTitle(pais.getName());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        finish();

        return;
    }


}
