package com.android2.continente.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android2.continente.R;
import com.android2.continente.model.Pais;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Callback;

import java.util.List;

public class PaisAdapter extends BaseAdapter {

    private List<Pais> paises;
    private Activity activity;

    public PaisAdapter(List<Pais> paises, Activity activity) {
        this.paises = paises;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return paises.size();
    }

    @Override
    public Object getItem(int position) {
        return paises.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = this.activity.getLayoutInflater().inflate(R.layout.layout_lista_paises, null);

        Pais pais = paises.get(position);

        final ImageView foto = view.findViewById(R.id.img_foto);



        Picasso.with(view.getContext())
                .load("https://www.countryflags.io/"+pais.getAlpha2Code()+"/flat/64.png")
                .error(R.drawable.ic_error_black_24dp)
                .placeholder(R.drawable.progress_animation)
                .into(foto, new Callback() {

                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        foto.setImageResource(R.drawable.ic_error_black_24dp);
                    }
                });


        TextView nomefantasia = view.findViewById(R.id.tv_nome);
        nomefantasia.setText(pais.getName());

        return view;
    }
}
