package com.android2.continente;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android2.continente.adapter.PaisAdapter;
import com.android2.continente.model.Pais;
import com.android2.continente.util.NetworkUtil;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PaisesActivity extends AppCompatActivity {

    String TAG = "PaisesActivity";
    String continenteSelecionado;
    Pais pais;
    ListView listView;
    TextView textView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paises);

        listView = findViewById(R.id.lst_paises);
        progressBar = findViewById(R.id.pg_loading);
        textView = findViewById(R.id.tv_information);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pais = (Pais) adapterView.getItemAtPosition(i);
                Log.i(TAG, "Pais Selecionado "+ pais.getName());
                showPais();
            }
        });

        Intent intent = getIntent();
        getSupportActionBar().setTitle(intent.getStringExtra("Continente"));
        continenteSelecionado = intent.getStringExtra("Continente");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        callSincronica();
    }

    public  void showPais(){
        Intent intent = new Intent(this, PaisActivity.class);
        intent.putExtra("pais", pais);
        startActivity(intent);
    }

    public void callSincronica() {
        URL url = NetworkUtil.buildUrlContinente(continenteSelecionado);
        Log.i(TAG, "Url Selecionado "+ url);
        BancoAsyncTask task = new BancoAsyncTask();
        task.execute(url);
    }

    class BancoAsyncTask extends AsyncTask<URL, Void, List<Pais>> {

        @Override
        protected List<Pais> doInBackground(URL... urls) {
            URL url = urls[0];
            String json = null;

            Gson gson = new Gson();
            Pais pais = new Pais();
            List<Pais> paises = new ArrayList<Pais>();
            String jpais = null;

            try {

                json = NetworkUtil.getResponseFromHttpUrl(url);
                JSONArray jsonArray = new JSONArray(json);

                for (int i = 0; i < jsonArray.length(); i++) {

                    jpais = jsonArray.get(i).toString();
                    pais = gson.fromJson(jpais, Pais.class);
                    paises.add(pais);
                }
                Log.i(TAG, "Primeiro Pais : " + paises.get(0).getName());

            }catch (IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return paises;
        }

        @Override
        protected void onPreExecute() {
            mostrarLoading();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<Pais> pais) {
            esconderLoading();
            if (pais == null) {
                textView.setText("Ocorreu um erro");
                textView.setVisibility(View.VISIBLE);
            } else {
                textView.setVisibility(View.INVISIBLE);
                PaisAdapter adapter = new PaisAdapter(pais, PaisesActivity.this);
                listView.setAdapter(adapter);
            }
        }
    }

    public void mostrarLoading() {
        listView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void esconderLoading() {
        listView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        finish();

        return;
    }

}
